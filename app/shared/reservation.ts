export class Reservation {
    guests: number;
    smoking: boolean;
    dateTime: Date;
}