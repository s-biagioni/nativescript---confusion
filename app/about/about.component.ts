import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';

import { Leader } from '../shared/leader';
import { LeaderService } from '../services/leader.service';

import { DrawerPage } from '../shared/drawer/drawer.page';

import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";


@Component({
    selector: 'app-about',
    moduleId: module.id,
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css']
  })

  export class AboutComponent extends DrawerPage  implements OnInit {

    historyTitle: string;
    ourHistory: string;

    leaderList: Leader[];
    errMess: string;

    constructor(private leaderService: LeaderService,
        private changeDetectorRef: ChangeDetectorRef,
        @Inject('baseURL') private baseURL) { 
          super(changeDetectorRef);
        }

    ngOnInit() { 
        this.historyTitle = "Our History";

        this.ourHistory = "Started in 2010, Ristorante con Fusion quickly established itself as a culinary icon par excellence in Hong Kong. With its unique brand of world fusion cuisine that can be found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.  Featuring four of the best three-star Michelin chefs in the world, you never know what will arrive on your plate the next time you visit us. \n \
        The restaurant traces its humble beginnings to The Frying Pan, a successful chain started by our CEO, Mr. Peter Pan, that featured for the first time the world's best cuisines in a pan.";
    
        this.leaderService.getLeaders()
        .subscribe(leaders => this.leaderList = leaders,
          errmess => this.errMess = <any>errmess);
    }

    onDrawerButtonTap(): void {
      const sideDrawer = <RadSideDrawer>app.getRootView();
      sideDrawer.showDrawer();
    }
  }