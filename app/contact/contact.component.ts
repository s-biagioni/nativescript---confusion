import { Component, OnInit, Inject, ChangeDetectorRef  } from '@angular/core';

import { DrawerPage } from '../shared/drawer/drawer.page';

import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import { TNSFontIconService } from 'nativescript-ngx-fonticon';
import * as Email from 'nativescript-email';


@Component({
    selector: 'app-contact',
    moduleId: module.id,
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
  })

  export class ContactComponent extends DrawerPage  implements OnInit {

    cardTitle: string;
    contactInformation: string;

    constructor(
      private changeDetectorRef: ChangeDetectorRef,
      private fonticon: TNSFontIconService,
      @Inject('baseURL') private baseURL) { 
        super(changeDetectorRef);
      }

    ngOnInit() { 
        this.cardTitle = "Contact Information";

        this.contactInformation = "\
        121, Clear Water Bay Road \n \
        Clear Water Bay, Kowloon \n \
        HONG KONG \n \
        Tel: +852 1234 5678 \n \
        Fax: +852 8765 4321 \n \
        Email:confusion@food.net";
    }

    onDrawerButtonTap(): void {
      const sideDrawer = <RadSideDrawer>app.getRootView();
      sideDrawer.showDrawer();
    }

    sendEmail() {

      Email.available()
        .then((avail: boolean) => {
          if (avail) {
            Email.compose({
              to: ['confusion@food.net'],
              subject: '[ConFusion]: Query',
              body: 'Dear Sir/Madam:'
            });
          }
          else
            console.log('No Email Configured');
        })
  
    }
    
  }