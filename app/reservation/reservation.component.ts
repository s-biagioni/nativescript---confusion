import { Component, OnInit, Inject, ChangeDetectorRef, ViewContainerRef } from '@angular/core';
import { TextField } from 'ui/text-field';
import { Switch } from 'ui/switch';
import { Validators, FormBuilder, FormGroup} from '@angular/forms';

import { DrawerPage } from '../shared/drawer/drawer.page';

import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ReservationModalComponent } from "../reservationmodal/reservationmodal.component";

import { Page } from "ui/page";
import { View } from "ui/core/view";

import { CouchbaseService } from '../services/couchbase.service';
import { Reservation } from '~/shared/reservation';


@Component({
    selector: 'app-reservation',
    moduleId: module.id,
    templateUrl: './reservation.component.html',
    styleUrls: ['./reservation.component.css']
})
export class ReservationComponent extends DrawerPage implements OnInit {

    reservation: FormGroup;

    // management of the views
    showReservationForm: boolean = true;
    showReservationSummary: boolean = false;

    reservationForm: View;
    reservationSummary: View;

    // values from  the form
    guests: number;
    smoking: boolean;
    dateTime: Date;

    reservations: Reservation[] = [];
    docId: string = "reservations";

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private modalService: ModalDialogService, 
        private vcRef: ViewContainerRef,
        private page: Page,
        private couchbaseService: CouchbaseService
        ) {
            super(changeDetectorRef);

            // constructing the form
            this.reservation = this.formBuilder.group({
                guests: 3,
                smoking: false,
                dateTime: ['', Validators.required]
            });

            this.reservations = [];

            let doc = this.couchbaseService.getDocument(this.docId);
            if( doc == null) {
              // pass  a JSON object to store the reservations
              this.couchbaseService.createDocument({"reservations": []}, this.docId);
              console.log("reservations after new couchbase initialization:" + JSON.stringify(this.reservations));
            }
            else {
              this.reservations = doc.reservations;

              console.log("reservations after old couchbase initialization:" + JSON.stringify(this.reservations));
              
              if( this.reservations == undefined){
                this.reservations = [];
                console.log("reservations after old couchbase initialization and second initialization:" + JSON.stringify(this.reservations));
              }
            }

    }

    ngOnInit() {    }

    createModalView(args) {

        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: args,
            fullscreen: false
        };

        this.modalService.showModal(ReservationModalComponent, options)
            .then((result: any) => {
                if (args === "guest") {
                    this.reservation.patchValue({guests: result});
                }
                else if (args === "date-time") {
                    this.reservation.patchValue({ dateTime: result});
                }
            });

    }

    onSmokingChecked(args) {
        let smokingSwitch = <Switch>args.object;

        // managing switch changes
        if (smokingSwitch.checked) {
            this.reservation.patchValue({ smoking: true });
        }
        else {
            this.reservation.patchValue({ smoking: false });
        }
    }

    onGuestChange(args) {
        let textField = <TextField>args.object;

        this.reservation.patchValue({ guests: textField.text});
    }

    onDateTimeChange(args) {
        let textField = <TextField>args.object;

        this.reservation.patchValue({ dateTime: textField.text});
    }

    onSubmit() {
        this.animation();

        this.updateCouchbaseDocument();
    }

    private updateCouchbaseDocument() {

        this.guests = this.reservation.get('guests').value;
        this.smoking = this.reservation.get('smoking').value;
        this.dateTime = this.reservation.get('dateTime').value;

        if (this.reservation.get('dateTime').value == undefined) {
            this.dateTime = new Date();
        }

        console.log("Values onSubmit(): " + JSON.stringify(this.reservation.value));

        this.reservations.push({
            guests: this.guests,
            smoking: this.smoking,
            dateTime: this.dateTime
        });

        this.couchbaseService.updateDocument(this.docId, { "reservations": this.reservations });
    }

    private animation() {

        this.reservationForm = this.page.getViewById<View>('reservationForm');
        this.reservationSummary = this.page.getViewById<View>('reservationSummary');

        this.showReservationForm = false;
        this.showReservationSummary = true;

        this.reservationSummary.animate({
            opacity: 0,
            scale: { x: 0, y: 0 }
        })
            .then(() => {
                this.reservationForm.animate({
                    opacity: 0,
                    scale: { x: 0, y: 0 },
                    duration: 500
                });
            })
            .then(() => {
                this.showReservationForm = false;
                this.showReservationSummary = true;
                this.reservationSummary.animate({
                    opacity: 1,
                    scale: { x: 1, y: 1 },
                    duration: 500
                });
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}